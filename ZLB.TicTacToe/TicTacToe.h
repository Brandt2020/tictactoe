#pragma once

#include <iostream>
#include <string>

using namespace std;

class TicTacToe
{

private:

	char m_board[10] = {'0','1','2','3','4','5','6','7','8','9' };
	int m_numTurns = 0;
	char m_playerTurn = '1';
	char m_winner;


	
public:

	//constructor
	TicTacToe() 
	{ 
	}

	//mutator

	void SetPlayer(char cPlayer) { m_playerTurn = cPlayer; }
	void SetWinner(char win) { m_winner = win; }

	//accessor

	char GetPlayerTurn() { return m_playerTurn; }
	char GetWinner() { return m_winner; }

	//other methods


	void DisplayBoard();

	bool IsValidMove(int move);
	
	void DetermineWinner();

	void Move(int move);

	
	void DisplayResult();

	bool IsOver();

};