#include "TicTacToe.h"


// TicTacToe::
void TicTacToe::DisplayBoard()
{

	cout << "\n" << m_board[1] << "|" << m_board[2] << "|" << m_board[3] << "\n______\n" << m_board[4] << "|" << m_board[5]
		<< "|" << m_board[6] << "\n______\n" << m_board[7] << "|" << m_board[8] << "|" << m_board[9] << "\n";
}

bool TicTacToe::IsValidMove(int move)
{

	if (m_board[move] == 'x' || m_board[move] == 'o')
	{
		return false;
	}
	else return true;
}

void TicTacToe::DetermineWinner()
{

	// If Player 1 wins
	if (m_board[1] == 'x' && m_board[2] == 'x' && m_board[3] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[4] == 'x' && m_board[5] == 'x' && m_board[6] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[7] == 'x' && m_board[8] == 'x' && m_board[9] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[1] == 'x' && m_board[4] == 'x' && m_board[7] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[2] == 'x' && m_board[5] == 'x' && m_board[8] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[3] == 'x' && m_board[6] == 'x' && m_board[9] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[1] == 'x' && m_board[5] == 'x' && m_board[9] == 'x')
	{
		SetWinner('1');
	}
	else if (m_board[3] == 'x' && m_board[5] == 'x' && m_board[7] == 'x')
	{
		SetWinner('1');
	}

	//If Player 2 wins
	if (m_board[1] == 'o' && m_board[2] == 'o' && m_board[3] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[4] == 'o' && m_board[5] == 'o' && m_board[6] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[7] == 'o' && m_board[8] == 'o' && m_board[9] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[1] == 'o' && m_board[4] == 'o' && m_board[7] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[2] == 'o' && m_board[5] == 'o' && m_board[8] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[3] == 'o' && m_board[6] == 'o' && m_board[9] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[1] == 'o' && m_board[5] == 'o' && m_board[9] == 'o')
	{
		SetWinner('2');
	}
	else if (m_board[3] == 'o' && m_board[5] == 'o' && m_board[7] == 'o')
	{
		SetWinner('2');
	}

}

void TicTacToe::Move(int move)
{
	if (GetPlayerTurn() == '1')
	{
		m_board[move] = 'x';
		SetPlayer('2');
		m_numTurns++;
	}
	else
	{
		m_board[move] = 'o';
		SetPlayer('1');
		m_numTurns++;
	}
}


void TicTacToe::DisplayResult()
{

	if (GetWinner() != '1' && GetWinner() != '2')
	{
		cout << "There are no more available spaces. The game is a tie.\n";
	}
	else
	{

		cout << "\nPlayer " << GetWinner() << " is the winner!\n";
	}
}

bool TicTacToe::IsOver()
{
	DetermineWinner();

	if (GetWinner() == '1' || GetWinner() == '2' || m_numTurns == 9)
	{
		return true;
	}
	else return false;


};